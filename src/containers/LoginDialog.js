import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';

import { withRouter } from 'react-router-dom';
import  {connect} from "react-redux"
import './Login.css';
import {
    FlatButton
} from 'material-ui';
import  Login from "./Login"
import  Signup from "./Signup"

class LoginDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLogin: true,
        }

    }


    render() {
        const actions = [
            <FlatButton
                label={this.state.isLogin ? "Not register?" : "Already a member?"}
                primary={true}
                keyboardFocused={true}
                onClick={()=>this.setState({isLogin: !this.state.isLogin})}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={()=>this.props.toggleLoginDiablog(false)}
            />
        ];

        return (
            <Dialog
                title="Login/Register"
                actions={actions}
                modal={true}
                bodyStyle={{overflowY:"auto"}}
                open={this.props.loginDialog}
                onRequestClose={this.handleClose}
            >
                {this.state.isLogin ? <Login
                    toggleLoginDiablog={()=>this.props.toggleLoginDiablog(false)}/> :
                    <Signup  toggleLoginDiablog={()=>this.props.toggleLoginDiablog(false)}/>}
            </Dialog>
        );
    }
}

LoginDialog = connect((store) => {
    return {...store}
})(LoginDialog);

export default withRouter(LoginDialog);
