import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ServiceDropdown from './serviceDropdown';

class SearchBar extends Component {
  render() {
    return (

          <div className="input-group">
            <input type="text" className="form-control" placeholder="Search for..." />
            <span className="input-group-btn">
            <div className="input-group-btn">
                <ServiceDropdown/>
            </div>
            </span>
          </div>

    );
  }
}

export default withRouter(SearchBar);
