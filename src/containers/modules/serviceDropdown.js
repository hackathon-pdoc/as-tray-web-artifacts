import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  DropdownButton,
  MenuItem,
} from 'react-bootstrap';
//import './SearchBar.css';

class ServiceDropdown extends Component {
    constructor(props) {
      super(props);

      this.state = {
        service: null,
        isLoading: true,
      };
    }

    async componentDidMount() {
        this.fetchService();
    }
    async fetchService() {
        var _this = this;

        const results =  fetch(window.location.href.split(window.location.pathname)[0] + '/service.json', {
          method: "GET"
        });

        // if (results.status !== 200) {
        //     throw new Error(await results.text());
        // }
        results.then((resp)=> resp.json()).then((respJson)=>{
            _this.setState({service: respJson});
            this.setState({isLoading: false});
            return respJson;
        });

    }
     renderServiceMenu() {
        let menu = [];
        if(!this.state.service) return;
        for(let i=0; i< this.state.service.length; i++) {
            let obj =  this.state.service[i];
                menu.push(<MenuItem key={i} eventKey={i}>{obj.name}</MenuItem>)
        }
        return menu;
    }
    render() {
        if(this.state.isLoading) {
            return (
                <DropdownButton title="Loading..." id="bg-nested-dropdown">
                </DropdownButton>
            );
        }
        return (
                <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                    {this.renderServiceMenu()}
                </DropdownButton>

        );
    }
}

export default withRouter(ServiceDropdown);
