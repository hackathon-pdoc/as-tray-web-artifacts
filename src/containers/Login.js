import React, { Component } from 'react';
import {
  FormGroup,
  FormControl,
  ControlLabel,
} from 'react-bootstrap';

import { withRouter } from 'react-router-dom';
import LoaderButton from '../components/LoaderButton';
import  {connect} from "react-redux"
import './Login.css';
import Snackbar from 'material-ui/Snackbar';
import  {login} from '../action/ActionAuthentication'
import  {getUserToken} from '../action/ActionAWSCognito'
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      username: '',
      password: '',
        hasError:false,
        errorMessage: "None"
    };
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  validateForm() {
    return this.state.username.length > 0
      && this.state.password.length > 0;
  }



  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  componentWillReceiveProps(nextProps) {
    if(typeof nextProps.Authentication.message !== "object" && nextProps.Authentication.message!=="") {
        if(!nextProps.CognitoTokens.hasSession && !nextProps.CognitoTokens.isFetching) {
            this.props.dispatch(getUserToken());
            nextProps.toggleLoginDiablog();
        }
    }
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    this.props.dispatch(login(this.state.username, this.state.password));
  }

  render() {
    console.debug(this.props);
    return (
      <div className="Login" style={{overflowY:"auto"}}>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.username}
              onChange={this.handleChange} />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password" />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={ ! this.validateForm() }
            type="submit"
            isLoading={this.props.Authentication.isFetching}
            text="Login"
            loadingText="Logging in…" />
        </form>
        <Snackbar
            open={this.props.Authentication.error}
            message={this.props.Authentication.message}
            autoHideDuration={4000}
            onRequestClose={this.handleRequestClose}
        />
      </div>
    );
  }
}

Login = connect((store) => {
    return {...store}
})(Login);

export default withRouter(Login);
