import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  HelpBlock,
  FormGroup,
  FormControl,
  ControlLabel,
} from 'react-bootstrap';
import {
  AuthenticationDetails,
  CognitoUserPool,
  CognitoUserAttribute,
} from 'amazon-cognito-identity-js';
import LoaderButton from '../components/LoaderButton';
import config from '../config.js';
import './Signup.css';
import Snackbar from 'material-ui/Snackbar';

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      username: '',
      password: '',
      confirmPassword: '',
      confirmationCode: '',
      newUser: null,
        hasError:false,
        errorMessage: "None"
    };
  }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
  validateForm() {
    let hasError = false, message = "";

    if(this.state.username.length<12 && this.validateEmail(this.state.username)) {
        hasError = true;
        message = "Invalid Username/Email";
    }

    if(this.state.password<8) {
        hasError = true;
        message = "invalid Mismatch";
    }
    if(this.state.password !== this.state.confirmPassword) {
        hasError = true;
        message = "Password Mismatch";
    }

    if(hasError) {
        this.setState({
            hasError: hasError,
            errorMessage: message
        });
    }

    return hasError;
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
        hasError: false,
    });
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    if(this.validateForm()) return;
    this.setState({ isLoading: true });

    try {
      const newUser = await this.signup(this.state.username, this.state.password);
      this.setState({
        newUser: newUser
      });
    }
    catch(e) {

    }

    this.setState({ isLoading: false });
  }

  handleConfirmationSubmit = async (event) => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.confirm(this.state.newUser, this.state.confirmationCode);
      const userToken = await this.authenticate(
        this.state.newUser,
        this.state.username,
        this.state.password
      );

      this.props.updateUserToken(userToken);
      this.props.history.push('/');
    }
    catch(e) {
      this.setState({ isLoading: false });
    }
  }

  signup(username, password) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    });
    const attributeEmail = new CognitoUserAttribute({ Name : 'email', Value : username });

    return new Promise((resolve, reject) => (
      userPool.signUp(username, password, [attributeEmail], null, (err, result) => {
        if (err) {
            this.setState({
                hasError: true,
                errorMessage: err.message

            });
          reject(err);
          return;
        }

        resolve(result.user);
      })
    ));
  }

  confirm(user, confirmationCode) {
    return new Promise((resolve, reject) => (
      user.confirmRegistration(confirmationCode, true, function(err, result) {
        if (err) {
            this.setState({
                hasError: true,
                errorMessage: err.message

            });
          reject(err);
          return;
        }
        this.props.toggleLoginDiablog(false);
        resolve(result);
      })
    ));
  }

  authenticate(user, username, password) {
    const authenticationData = {
      Username: username,
      Password: password
    };
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) => (
      user.authenticateUser(authenticationDetails, {
        onSuccess: (result) => resolve(result.getIdToken().getJwtToken()),
        onFailure: (err) => reject(err),
      })
    ));
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>Confirmation Code</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            value={this.state.confirmationCode}
            onChange={this.handleChange} />
          <HelpBlock>Please check your email for the code.</HelpBlock>
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={ ! this.validateConfirmationForm() }
          type="submit"
          isLoading={this.state.isLoading}
          text="Verify"
          loadingText="Verifying…" />
      </form>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="username" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={this.state.username}
            onChange={this.handleChange} />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={this.state.password}
            onChange={this.handleChange}
            type="password" />
        </FormGroup>
        <FormGroup controlId="confirmPassword" bsSize="large">
          <ControlLabel>Confirm Password</ControlLabel>
          <FormControl
            value={this.state.confirmPassword}
            onChange={this.handleChange}
            type="password" />
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={ this.state.hasError }
          type="submit"
          isLoading={this.state.isLoading}
          text="Signup"
          loadingText="Signing up…" />
      </form>
    );
  }

  render() {
    return (
      <div className="Signup" style={{overflowY:"auto"}}>
        { this.state.newUser === null
          ? this.renderForm()
          : this.renderConfirmationForm() }
        <Snackbar
            open={this.state.hasError}
            message={this.state.errorMessage}
            autoHideDuration={4000}
            onRequestClose={this.handleRequestClose}
        />
      </div>
    );
  }
}

export default withRouter(Signup);
