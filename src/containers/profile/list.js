import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ProfileSidebar from './sidebar.js';
import './sidebar.css';

class ListProfile extends Component {
  render() {
      console.debug(this.props);
    return (
      <div className="profile">
        <div className="row profile">
    		<div className="col-sm-9">
                <div className="profile-content">
        			   Some user related content goes here...
                </div>
    		</div>
    	</div>
      </div>
    );
  }
}

export default withRouter(ListProfile);
