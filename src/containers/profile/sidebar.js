import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router-dom';

import {s3UploadProfile} from "../../libs/awsLib";
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import makeSelectable from 'material-ui/List/makeSelectable';
import SvgIconFace from 'material-ui/svg-icons/action/face';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentArchive from 'material-ui/svg-icons/content/archive';
let SelectableList = makeSelectable(List);
function wrapState(ComposedComponent) {
    return class SelectableList extends Component {
        static propTypes = {
            children: PropTypes.node.isRequired,
            defaultValue: PropTypes.number.isRequired,
        };

        componentWillMount() {
            this.setState({
                selectedIndex: this.props.defaultValue,
            });
        }

        handleRequestChange = (event, index) => {
            this.setState({
                selectedIndex: index,
            });
        };

        render() {
            return (
				<ComposedComponent
					value={this.state.selectedIndex}
					onChange={this.handleRequestChange}
				>
                    {this.props.children}
				</ComposedComponent>
            );
        }
    };
}

SelectableList = wrapState(SelectableList);

const style = {margin: 5};

class ProfileSidebar extends Component {
    constructor(props) {
      super(props);
		console.debug("ProfileSidebar");
		console.log(props.profile);
      this.state = {
        isLoading: false,
        profile: props.profile,
      };
        this.handleNavLink = this.handleNavLink.bind(this);
    }
    handleNavLink = (event) => {
        event.preventDefault();
        this.props.history.push(event.currentTarget.getAttribute('href'));
    }

  	render() {
        let user_avartar = {
            color:"#444",
            icon: <SvgIconFace />
        };
		let current = this.props.match.path;
		let value = 0;

		switch (current) {
			case "/starred":
				value = 1;
				break
			case "/archived":
				value = 2;
				break
		}


        if(this.state.profile.profile_pic!==null) {
            user_avartar = {
            	src: this.state.profile.profile_pic
			}
		}
    	return (
			<div className="col-md-3 col-sm-4 hidden-xs ">
					<List >
						<ListItem
							onClick={this.handleNavLink}
							href="/profile"
							leftAvatar={
								<Avatar {...user_avartar}/>
							}
						>
                            {this.state.profile.username}
						</ListItem>
						<Divider inset={true}/>
						<SelectableList defaultValue={value}>
							<ListItem insetChildren={true} value={0} primaryText="Up Coming" leftIcon={<ContentInbox />} />
							<ListItem insetChildren={true} value={1} primaryText="Starred" leftIcon={<ActionGrade />} />
							<ListItem insetChildren={true} value={2} primaryText="Archives" leftIcon={<ContentArchive />} />
						</SelectableList>
						<Divider inset={true}/>
						<List>
							<ListItem insetChildren={true} onClick={this.props.logout} primaryText="Logout" leftIcon={<ContentSend />} />
						</List>
					</List>


			</div>
		);
    }
}

export default withRouter(ProfileSidebar);
