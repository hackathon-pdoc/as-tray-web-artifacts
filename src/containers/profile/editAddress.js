import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './sidebar.css';

class EditAddress extends Component {
    constructor(props) {
        super(props);

        this.file = null;

        this.state = {
            latlong: "",
            address1: "",
            address2: "",
            city: "",
            zip: ""
        };
    }
    handleChange(field, e) {
        var nextState = {};
        nextState[field] = e.target.value;
        this.setState(nextState);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label className="col-md-3 control-label">Address 1:</label>
                    <div className="col-md-8">
                        <input className="form-control" onSubmit={this.handleChange.bind(this, "address1")} value="" type="text"/>
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-md-3 control-label">Address 2:</label>
                    <div className="col-md-8">
                        <input className="form-control" onSubmit={this.handleChange.bind(this, "address2")} value="" type="text"/>
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-md-3 control-label">City:</label>
                    <div className="col-md-8">
                        <input className="form-control" onSubmit={this.handleChange.bind(this, "city")} value="" type="text"/>
                    </div>
                    <label className="col-md-3 control-label">Zip:</label>
                    <div className="col-md-8">
                        <input className="form-control" onSubmit={this.handleChange.bind(this, "zip")} value="" type="text"/>
                    </div>
                </div>
            </div>

        );
    }
}

export default withRouter(EditAddress);