import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './sidebar.css';

class EditContact extends Component {
    constructor(props) {
        super(props);

        this.file = null;
        this.state = {
            phone:  "",
            mobile: "",
            email: "",
        };
    }
    componentDidMount() {
        if(this.props.profile) {
            let profile = this.props.profile || {
                phone: "",
                email: ""
            };
            this.setState({
                phone: profile.phone,
                email: profile.email,
            });
        }
    }
    handleChange(field, e) {
        var nextState = {};
        nextState[field] = e.target.value;
        this.setState(nextState);
        this.props.handleChange(field, e.target.value)
    }


    render() {
        console.log(this.props.profile);
        return (
            <div>
                <div className="form-group">
                    {/*<label className="col-md-3 control-label">Mobile:</label>
                    <div className="col-md-3">
                        <input className="form-control" value={this.state.mobile} onChange={this.handleChange.bind(this, "mobile")} type="phone"/>
                    </div>*/}
                    <label className=" col-md-3 control-label">Phone:</label>
                    <div className="col-md-8">
                        <input className="form-control" value={this.state.phone} onChange={this.handleChange.bind(this, "phone")} type="phone"/>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-md-3 control-label">Email:</label>
                    <div className="col-md-8">
                        <input className="form-control" value={this.state.email} onChange={this.handleChange.bind(this, "email")} type="email"/>
                    </div>
                </div>

            </div>
        );
    }
}

export default withRouter(EditContact);