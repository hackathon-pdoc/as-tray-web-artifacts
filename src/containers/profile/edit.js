import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './sidebar.css';
import ProfileSidebar from './sidebar.js';
import EditAddress from './editAddress.js';
import EditContact from './editContact.js';
import { invokeProfile } from '../../libs/awsLib';
import Paper from 'material-ui/Paper';

class EditProfile extends Component {
    contact = {}
    address = {}
    constructor(props) {
        super(props);
        this.file = null;

        this.state = {
            firstName: "",
            lastName: "",
            contact: {},
            address: {}
        };
    }
    componentDidMount() {
        if(this.props.profile) {
            let profile = this.props.profile || {
                firstName: "",
                lastName: "",
            };
            this.setState({
                firstName: profile.firstName,
                lastName: profile.lastName,
            });
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        var body = {
            ...this.state,

        };

        console.log(body)

        return invokeProfile({path: '/profile', method: "POST", body: body}, this.props.userToken);
    }
    handleContactChange(field, value) {
        // this.setState({contact{[field]: value});
        this.setState({contact: {
            [field]:value
        }});

        console.log(this.state.contact)
    }
    handleAddressChange(field, value) {
        this.address[field] = value;
    }
    handleChange(field, e) {
        var nextState = {}
        nextState[field] = e.target.value;
        this.setState(nextState)
    }
    render() {
        console.debug(this.props);

        return (
            <div className="container edit-profile-container">
                <form className="form-horizontal" onSubmit={this.handleSubmit} role="form-profile">
                    <Paper className="profile-sidebar col-sm-12" zDepth={2}>

                        <h3>Personal info</h3>
                            <div className="form-group">
                                <label className="col-md-3 control-label">First name:</label>
                                <div className="col-md-8">
                                    <input className="form-control" value={this.props.profile.first_name} onChange={this.handleChange.bind(this, "firstName")}  type="text"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">Last Name:</label>
                                <div className="col-md-8">
                                    <input className="form-control" value={this.props.profile.last_name} onChange={this.handleChange.bind(this, "lastName")} type="text"/>
                                </div>
                            </div>
                    </Paper>

                    <Paper className="profile-sidebar col-sm-12" zDepth={2}>
                            <h3>Contact Details</h3>
                            <EditContact handleChange={this.handleContactChange.bind(this)} {...this.props}/>
                    </Paper>

                    <Paper className="profile-sidebar col-sm-12" zDepth={2}>
                            <h3>Location</h3>
                            <EditAddress handleChange={this.handleAddressChange} {...this.props}/>

                    </Paper>
                    <div className="form-group">
                        <label className="col-md-3 control-label"></label>
                        <div className="col-md-8">
                            <input className="btn btn-primary" value="Save Changes" type="submit"/>
                            <span></span>
                            <input className="btn btn-default" value="Cancel" type="reset"/>
                        </div>
                    </div>
                </form>
            </div>

        );
    }
}

export default withRouter(EditProfile);
