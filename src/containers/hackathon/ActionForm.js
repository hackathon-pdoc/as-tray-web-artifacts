import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import TextField from 'material-ui/TextField';
import {invokeCommentsAPI} from "../../libs/awsLib";


export default class ListByType extends Component {

    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    async submitForm() {
        var payload = {
            comment: this.state.comment,
            email: this.state.email,
            articleType: this.props.ArticleType,
            articleId: this.props.ArticleId,
            amount: this.state.amount
        };


        let data = await invokeCommentsAPI({body: payload, method: "PUT"}, null);
        console.debug(data);
    }
    constructor(props) {
        super(props)
        console.debug(props);
        this.isLoggedId=false;
        this.state= {
            isLoading:true,
            comment: "",
            email: props.profile.email || "",
            amount: 1
        };
        this.handleChange = this.handleChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {

    }

    renderVNT() {
        return(
            <div className="col-md-12">
                <TextField
                    fullWidth={true}
                    id="comment"
                    hintText="Add your Comment"
                    multiLine={true}
                    rows={2}
                    rowsMax={4}
                    floatingLabelText="Comment"
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />
                <TextField
                    fullWidth={true}
                    id="email"
                    hintText="juan@example.com"
                    floatingLabelText="Email"
                    value={this.state.email}
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />

            </div>
        )
    }
    renderSGT() {
        return(
            <div className="col-md-12">
                <TextField
                    hintText="Add your Comment"
                    id="comment"
                    multiLine={true}
                    fullWidth={true}
                    rows={2}
                    rowsMax={4}
                    floatingLabelText="Comment"
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />
                <TextField
                    hintText="juan@example.com"
                    id="email"
                    value={this.state.email}
                    fullWidth={true}
                    floatingLabelText="Email"
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />

            </div>
        )
    }
    renderCTR() {
        return(
            <div className="col-md-12">
                <TextField
                    hintText="Add your Comment"
                    multiLine={true}
                    id="comment"
                    fullWidth={true}
                    rows={2}
                    rowsMax={4}
                    floatingLabelText="Comment"
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />
                <TextField
                    hintText="500.00"
                    floatingLabelText="Amount"
                    id="amount"
                    floatingLabelFixed={true}
                    fullWidth={true}
                    onChange={this.handleChange}
                />
                <TextField
                    hintText="juan@example.com"
                    id="email"
                    value={this.state.email}
                    floatingLabelText="Email"
                    floatingLabelFixed={true}
                    fullWidth={true}
                    onChange={this.handleChange}
                />
            </div>
        )
    }

    render() {
        var showForm=null;
        console.debug(this.props);
        switch (this.props.ArticleType) {
            case "VNT":
                showForm=this.renderVNT()
                break;
            case "CTR":
                showForm=this.renderCTR()

                break;
            default:
                showForm=this.renderSGT()


        }
        return showForm;
    }
}
