import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {invokeArticleAPI} from '../../libs/awsLib'
import LinearProgress from 'material-ui/LinearProgress';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

export default class ListByType extends Component {

    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }

    constructor(props) {
        super(props)
        console.debug(props);
        this.isLoggedId=false;
        this.state= {
            isLoading:true,
            article: []
        }

    }
    async articles(id) {
        let data = await invokeArticleAPI({path: '/articles/'+id}, null);

        return data;
    }
    async componentDidMount() {
        var _this = this;
        if (this.props.userToken !== null) {
            this.isLoggedId = true
        }

        this.setState({ isLoading: true });

        try {
            const results = await this.articles(this.props.articleType);


            this.setState({ article: results,  isLoading: false  });
        }
        catch(e) {
            this.setState({isLoading:false});
        }

    }
    createMarkup(data) {
        return {__html: data};
    }


    render() {
        if(this.state.article.length==0 || this.state.isLoading) return null;
        console.debug(this.state.article.length);
        const {ArticleID, ArticleType, Title, Descr, Location, Target, EventDate, ExpireDate, TotalContribution, Banners, CreatedByUser, CreatedDateTime, ModifiedDateTime} = this.state.article;
        var TotalContributionText = "",
            TotalContributionType = TotalContribution,
            arrayLenght = this.state.article.length;
        console.debug(arrayLenght);
        return (
            <div className="clearfix">
                <GridList
                    cols={2}
                    cellHeight={200}
                    padding={1}
                    className="col-xs-12"
                >
                    {this.state.article.map((article, i) => {
                        let col = i % 5;
                        const {
                            ArticleID,
                            ArticleType,
                            Title,
                            Descr,
                            Location,
                            Target,
                            EventDate,
                            ExpireDate,
                            TotalContribution,
                            Banners,
                            CreatedByUser,
                            CreatedDateTime,
                            ModifiedDateTime} = article,
                            banner = JSON.parse(Banners)[0].Location;
                            let rand = Math.floor(Math.random()*(1-0+1)+0);
                        return (
                        <GridTile
                            key={ArticleID}
                            title={Title}
                            actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
                            actionPosition="left"
                            titlePosition="top"
                            titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
                            cols={rand}
                            rows={col}
                        >
                            <img src={banner} />
                        </GridTile>
                        )
                    }
                    )}
                </GridList>
            </div>
        );
    }
}
