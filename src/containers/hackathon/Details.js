import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import ImageGallery from 'react-image-gallery';
import {invokeArticleAPI} from '../../libs/awsLib'
import LinearProgress from 'material-ui/LinearProgress';
import ActionForm from './ActionForm';
import ListByType from './ListByType';
import  './Details.css';
import moment from 'moment'


class Details extends Component {
    images = [
        {
            original: 'http://lorempixel.com/1000/600/nature/1/',
            thumbnail: 'http://lorempixel.com/250/150/nature/1/',
        },
        {
            original: 'http://lorempixel.com/1000/600/nature/2/',
            thumbnail: 'http://lorempixel.com/250/150/nature/2/'
        },
        {
            original: 'http://lorempixel.com/1000/600/nature/3/',
            thumbnail: 'http://lorempixel.com/250/150/nature/3/'
        }
    ];
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
    submitForm() {
        this.actionForm.submitForm();
    }
    constructor(props) {
        super(props)
        console.debug(props);
        this.isLoggedId=false;
        this.state= {
            isLoading:false,
            article: []
        }
        this.submitForm = this.submitForm.bind(this);

    }
    async articles(id) {
        let data = await invokeArticleAPI({path: '/article/'+id}, null);

        return data;
    }
    async componentDidMount() {
        var _this = this;
        if (this.props.userToken !== null) {
            this.isLoggedId = true
        }

        this.setState({ isLoading: true });

        try {
            const results = await this.articles(this.props.match.params.id);
            if(results.length===0) this.props.history.push('/');
            if(results[0].Banners.length!==0) this.images = [];
            JSON.parse(results[0].Banners).forEach((i)=> {
                _this.images.push({
                    original: i.Location,
                    thumbnail: i.Location
                });
            });

            this.setState({ article: results[0],  isLoading: false  });
        }
        catch(e) {
            this.setState({isLoading:false});
        }

    }
    createMarkup(data) {
        return {__html: data};
    }


    render() {
        if(this.state.article.length==0) return null;

        const {ArticleID, ArticleType, Title, Descr, Location, Count, Target, EventDate, ExpireDate, TotalContribution, Banners, CreatedByUser, CreatedDateTime, ModifiedDateTime} = this.state.article;
        var TotalContributionText = "",
            TotalContributionType = TotalContribution,
            buttonLabel = "Sign Up",
            willExpire=moment(EventDate, "YYYYMMDD").fromNow(),
            willExpires=moment(EventDate, "YYYYMMDD").fromNow().indexOf("in")<0,
            willExpireText="";

        switch (ArticleType) {
            case "VNT":
                TotalContributionText = "Current Volunteers";
                buttonLabel= "Volunteer";
                willExpireText=willExpires? "Has Expired " : "Will expire";
                break;
            case "CTR":
                TotalContributionText = "Total Contribution";
                TotalContributionType = "$ " + (Math.round(TotalContributionType*Math.pow(10,2))/Math.pow(10,2)).toFixed(2);
                buttonLabel= "Contribute";
                willExpireText=willExpires ? "Has Ended " : "Will End ";
                break;
            default:
                TotalContributionText = "Total Signatures";
                willExpireText=willExpires ? "Has Expired " : "Will expire";

        }


        return (
            <div id="create-post-container">
                <div className="row">
                    <div className="col-xs-6">

                        <ImageGallery
                            items={this.images}
                            slideInterval={2000}
                            showThumbnails={false}
                            showFullscreenButton={false}
                            showPlayButton={false}
                        />
                    </div>
                    <Card  className="col-xs-6">
                        <CardHeader
                            className="card-header-details-summary"
                            title={Title}
                            subtitle={CreatedByUser}
                        />

                        <CardText>
                            <div className="details-data">
                                <p>{willExpireText}</p>
                                <h3>{willExpire}</h3>
                            </div>
                            <h4>{TotalContributionText} <strong style={{float: "right"}}>{TotalContributionType}</strong></h4>
                            <LinearProgress mode="determinate" value={TotalContribution} max={Target}/>
                        </CardText>
                        <CardActions>

                        </CardActions>
                    </Card>

                </div>
                <div className="row">
                    <div className="col-xs-9">
                        <div dangerouslySetInnerHTML={this.createMarkup(Descr)} ></div>
                    </div>
                    <div className="col-xs-3 row">
                        <Card className="side-card row">
                            <CardText className="row">
                                <ActionForm  ArticleId={ArticleID} ref={instance => { this.actionForm = instance; }} {...this.props} ArticleType={ArticleType}/>
                            </CardText>
                            <CardActions className="col-sm-12 clearfix">
                                <div className="col-sm-12">
                                    <RaisedButton
                                        primary={true}
                                        onClick={this.submitForm}
                                        fullWidth={true}
                                        label={buttonLabel}
                                    />
                                </div>
                                <div className="col-sm-12 clearfix">
                                    <FlatButton
                                        label="Add to Calendar"
                                        fullWidth={true}
                                        icon={<span className="glyphicon glyphicon-calendar" />}
                                    />
                                </div>
                            </CardActions>
                        </Card>
                        {(ArticleType==="VNT") ?
                        <Card className="side-card row">
                            <CardText className="row">
                                <ActionForm {...this.props} ArticleType={ArticleType} ArticleId={ArticleID}/>
                            </CardText>
                            <CardActions className="col-sm-12 clearfix">
                                <div className="col-sm-12">
                                    <RaisedButton
                                        primary={true}
                                        fullWidth={true}
                                        label={buttonLabel}
                                    />
                                </div>
                                <div className="col-sm-12 clearfix">
                                    <FlatButton
                                        label="Add to Calendar"
                                        fullWidth={true}
                                        icon={<span className="glyphicon glyphicon-calendar" />}
                                    />
                                </div>
                            </CardActions>
                        </Card> : null}

                    </div>
                </div>
                <div className="row">
                    <h3>Comments</h3>
                    <Divider />
                </div>
                <div className="col-sm-12">

                    <Card className="side-card row">
                        <CardHeader
                            title="jatazoulja@gmail.com"
                            subtitle="May 4, 2016"
                            avatar="images/jsa-128.jpg"
                        />
                        <CardText>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                            Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                            Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                        </CardText>
                    </Card>


                    <Card className="side-card row">
                        <CardHeader
                            title="jatazoulja@gmail.com"
                            subtitle="May 4, 2016"
                            avatar="images/jsa-128.jpg"
                        />
                        <CardText>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                            Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                            Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                        </CardText>
                    </Card>


                    <Card className="side-card row">
                        <CardHeader
                            title="jatazoulja@gmail.com"
                            subtitle="May 4, 2016"
                            avatar="images/jsa-128.jpg"
                        />
                        <CardText>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                            Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                            Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                        </CardText>
                    </Card>


                    <Card className="side-card row">
                        <CardHeader
                            title="jatazoulja@gmail.com"
                            subtitle="May 4, 2016"
                            avatar="images/jsa-128.jpg"
                        />
                        <CardText>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                            Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                            Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                        </CardText>
                    </Card>


                </div>
                <div className="row">
                    <h3>Similar Campaigns:</h3>
                    <Divider />

                </div>
                <div className="row view-similar">
                    <ListByType className="" articleType={ArticleType}/>
                </div>
            </div>
        );
    }
}
export default withRouter(Details);
