import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import  {connect} from "react-redux"
import {getUserLocation} from '../../action/ActionMyLocation';
import ListMapMarker from './ListMapMarker'
import Commnets from './Comments'
import Geosuggest from "react-geosuggest"
import {setFollowLocation} from '../../action/ActionMyLocation'

class MyLocation extends Component {
    constructor(props) {
        super(props);

        this.state={
            myLocation: false
        };
        this.allowMyLocation = this.allowMyLocation.bind(this);
        this.updateCenter = this.updateCenter.bind(this);
    }

    allowMyLocation() {
        this.props.dispatch(getUserLocation());
    }
    onSuggestSelect(obj) {
        this.updateCenter([obj.location.lat, obj.location.lng]);
    }
    renderSearhBar() {
        const {MyGeoLocation} = this.props;

        return (
            <div className="lander text-center">
                <Geosuggest onSuggestSelect={(obj)=>this.onSuggestSelect(obj)} />
                <a onClick={()=>this.updateCenter([MyGeoLocation.coords.latitude, MyGeoLocation.coords.longitude])}>Center to my location</a>
            </div>
        );
    }

    renderMyLocation() {
        return (
            <div className="lander text-center">
                <a onClick={this.allowMyLocation}>Please Enable geolocation.</a>

            </div>
        );
    }
    updateCenter(center) {
        this.props.dispatch(setFollowLocation(true));
        this.props.updateCenter(center)
    }
    render() {
        const {MyGeoLocation, Markers} = this.props;
        return (
            <div style={{backgroundColor:'white'}}>
                {!MyGeoLocation.isLocationAllowed ? this.renderMyLocation() : this.renderSearhBar()}
                <ListMapMarker
                    toggleLoginDiablog={(bol)=>this.props.toggleLoginDiablog(bol)}
                    updateCenter={this.updateCenter}
                    markers={Markers.markers}/>
                {"index" in Markers && "comments" in Markers.markers[Markers.index] ? <Commnets comments={Markers.markers[Markers.index].comments}/> : null}
            </div>

        );
    }
}

MyLocation = connect((store) => {
    return {...store}
})(MyLocation);


export default MyLocation;
