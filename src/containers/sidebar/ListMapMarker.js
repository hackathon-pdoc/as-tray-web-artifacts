import React from 'react';
import {
    Step,
    Stepper,
    StepButton,
    StepContent,
} from 'material-ui/Stepper';
import {
    RaisedButton,
    FlatButton,
    Paper,
    Divider,
    Dialog,
    TextField
} from 'material-ui';
import {connect} from "react-redux"
import { Rating } from 'material-ui-rating'
import {createMarkerRating, getMarkerRatings} from '../../action/ActionMarkers'
import {setFollowLocation} from '../../action/ActionMyLocation'

/**
 * A basic vertical non-linear implementation
 */
class ListMapMarker extends React.Component {

    state = {
        stepIndex: 0,
        openDialog: false,
        rate: 0,
        marker_id:0,
        comment: ""
    };

    handleNext = () => {
        const {stepIndex} = this.state;
        if (stepIndex < 2) {
            this.setState({stepIndex: stepIndex + 1});
        }
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    renderStepActions(step) {
        return (
            <div style={{margin: '12px 0'}}>
                <RaisedButton
                    label="Next"
                    disableTouchRipple={true}
                    disableFocusRipple={true}
                    primary={true}
                    onClick={this.handleNext}
                    style={{marginRight: 12}}
                />
                {step > 0 && (
                    <FlatButton
                        label="Back"
                        disableTouchRipple={true}
                        disableFocusRipple={true}
                        onClick={this.handlePrev}
                    />
                )}
            </div>
        );
    }
    setRating(rates, id) {
        if(this.state.openDialog) {
            this.setState({
                rate: rates
            });
            return
        }
        const {CognitoTokens} = this.props;
        if (!CognitoTokens.hasSession) {
            this.props.toggleLoginDiablog(true)
            return
        }

        this.setState({
            openDialog:true,
            rate: rates,
            marker_id: id
        });

        return null;
    }
    steps() {
        const count = this.props.markers.length;
        const {Markers, dispatch} =  this.props;

        return this.props.markers.map((obj, i)=>
            (
                <Step key={obj.id}>
                <StepButton onClick={() => {
                    //get comment
                    if(!("comment" in obj)) dispatch(getMarkerRatings(obj.id));
                    dispatch(setFollowLocation(false));
                    this.setState({stepIndex: i});
                    this.props.updateCenter([obj.lat, obj.lng]);
                }}>
                    {obj.name.substring(0,24)+"..."}
                </StepButton>
                <StepContent>
                    <Paper style={{padding:10}} zDepth={3} rounded={false} >
                    <div className="row">
                        <h6 className="col-xs-6">
                            by: {obj.created_by}
                        </h6>
                        <h6 className="text-right col-xs-6">
                            {(obj.distance*1.60934).toFixed(2)}km away
                        </h6>
                    </div>
                    <p>
                        {obj.address}
                    </p>
                    <p>
                        {obj.description}
                    </p>

                    <Divider />
                    <div className="text-center row">
                        <Rating
                            value={obj.rating || 0}
                            max={5}
                            onChange={(value) => this.setRating(value, obj.id)}
                        />
                    </div>
                    </Paper>
                </StepContent>
                </Step>
            )
        )
    }
    render() {
        const {stepIndex} = this.state;
        const {CognitoTokens} =  this.props;
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={true}
                onClick={()=>this.setState({openDialog: false})}
            />,
            <RaisedButton
                label="Submit"
                primary={true}
                onClick={()=> {
                    console.debug(CognitoTokens.token);
                    this.props.dispatch(createMarkerRating({
                        comment: this.state.comment,
                        rate: this.state.rate,
                        marker_id: this.state.marker_id
                    }, CognitoTokens.token));
                    this.setState({openDialog: false})
                }}
            />
        ];
        return (
            <div style={{margin: 'auto', overflowY:"auto"}}>
                <Stepper
                    style={{maxWidth: 380, maxHeight: (document.body.clientHeight/2)-80, margin: 'auto'}}
                    activeStep={stepIndex}
                    linear={false}
                    orientation="vertical"
                >

                    {this.steps()}
                </Stepper>
                <Dialog
                    title="Rate this place"
                    actions={actions}
                    modal={true}
                    open={this.state.openDialog}
                >
                    <Rating
                        value={this.state.rate}
                        max={5}
                        onChange={(value) => this.setRating(value)}
                    />
                    <TextField
                        hintText="Hint Text"
                        floatingLabelText="Fixed Floating Label Text"
                        floatingLabelFixed={true}
                        value={this.state.comment}
                        onChange={(e, value)=> {this.setState({comment:value})}}
                    />
                </Dialog>
            </div>
        );
    }
}
ListMapMarker = connect((store) => {
    return {...store}
})(ListMapMarker);
export default ListMapMarker;