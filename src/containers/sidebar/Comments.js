import React, { Component } from 'react';
import {
    Paper,
    Divider
} from 'material-ui';
import { Rating } from 'material-ui-rating'


class Comments extends React.Component {
    renderComments() {
        return this.props.comments.map((obj) => {

            return (<div className="col-sm-12 hidden-xs" key={obj.id}>
                <Rating
                    itemStyle={{height:20, width:20}}
                    readOnly={true}
                    value={obj.rate || 0}
                    max={5}/>

                <h5>{obj.username}</h5>
                <p>{obj.comment}</p>
               <Divider/>
            </div>)

        });
    }

    render() {
        return(
            <Paper style={{marginTop:10}} className="col-sm-12">
                {this.renderComments()}
            </Paper>
        )
    }
}

export default Comments