import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import {
    PageHeader,
    ListGroup,
    ListGroupItem,
} from 'react-bootstrap';

class DetailsPost extends Component {

    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className="Home">
                {this.props.match.params.id}
            </div>
        );
    }
}

export default withRouter(DetailsPost);
