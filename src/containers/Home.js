import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import './Home.css';
import { invokeArticleAPI} from '../libs/awsLib'
import ListService from '../containers/service/List';
import MapComponent from '../components/MapComponent';
import {connect} from "react-redux"
import {getMarkerLocation} from '../action/ActionMarkers';
import {getUserLocation} from '../action/ActionMyLocation';
import {MyLocation} from "../containers/sidebar";
import CreateMarkerDialog from '../components/CreateMarkerDialog';
import LoginDialog from './LoginDialog'
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
        articles: [],
        center:[],
        openCreateMarkerDialog: false,
        locations: {},
        payload:{},
        loginDialog:false
    };
    this.updateCenter = this.updateCenter.bind(this);
  }
  async articles() {
      let data = await invokeArticleAPI({method: "GET", body:{}}, this.props.userToken);
      console.debug(data);
      return data;
  }
    toggleDialog(bol, obj) {
      if(bol) {
          fetch(
              "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +obj.lat+ "," + obj.lng,
              {method: "GET"}
          )
          .then((resp) => resp.json())
          .then((resp) => {
              this.setState({
                  openCreateMarkerDialog:bol,
                  locations: resp.results,
                  payload: obj
              })

           });
          return
      }

      this.setState({
          openCreateMarkerDialog:bol
      })
    }
    async componentWillMount() {

        if (navigator.geolocation) {
            this.props.dispatch(getUserLocation((location)=> {
                this.updateCenter([location.coords.latitude, location.coords.longitude])
                this.props.dispatch(getMarkerLocation({}, location.coords));
            }));
        }
    }

    toggleLogin(bol) {
        this.setState({
            loginDialog:bol
        })
    }
    async componentDidMount() {
    if (this.props.userToken === null) {
      return;
    }

    this.setState({ isLoading: true });

    try {
      const results = await this.articles();
      this.setState({ articles: results });
    }
    catch(e) {
      // alert(e);
    }

    this.setState({ isLoading: false });
  }
    updateCenter(center) {
        const {MyGeoLocation} = this.props;
        if(!MyGeoLocation.isLocationAllowed) return;

        console.debug(center);
        this.setState({
            center: center
        })
    }
    createMarker(bol, obj) {
      console.log(obj);
      this.toggleDialog(true, obj);
    }
  renderLander() {
    return (
      <div className="lander">
        <h1>Scratch</h1>
        <p>A simple note taking app</p>
        <div>
          <Link to="/login" className="btn btn-info btn-lg">Login</Link>
          <Link to="/signup" className="btn btn-success btn-lg">Signup</Link>
        </div>
      </div>
    );
  }

  renderNotes() {
    return (
      <div className="row">
        <ListService {...this.props}/>
      </div>
    );
  }

  render() {

      return (
          <div style={{height:this.props.clientHeight}}>
              <div className="mapContainer col-sm-8 col-md-8">
                  <MapComponent
                      isMarkerShown
                      createMarker={(bol, obj)=>this.createMarker(bol, obj)}
                      center={this.state.center}
                      toggleLoginDiablog={(bol)=>this.toggleLogin(bol)}
                  />
                  <CreateMarkerDialog
                      center=""
                      payload={this.state.payload}
                      locations={this.state.locations}
                      open={this.state.openCreateMarkerDialog}
                      toggleDialog={(bol)=>this.toggleDialog(bol)}
                  />
              </div>
              <div  className="mapContainer col-sm-4 col-md-4">
                    <MyLocation
                        toggleLoginDiablog={(bol)=>this.toggleLogin(bol)}
                        updateCenter={this.updateCenter}
                    />
              </div>
              <LoginDialog loginDialog={this.state.loginDialog} toggleLoginDiablog={(bol)=>this.toggleLogin(bol)}/>
          </div>
    );
  }
}

Home = connect((store) => {
    console.log(store);
    return {...store}
})(Home);


export default withRouter(Home);
