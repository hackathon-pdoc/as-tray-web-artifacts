import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  Tabs,
  Tab,
} from 'react-bootstrap';
import './service.css';

class ServiceIndex extends Component {

  render() {
    return (
    <div className="content-wrapper">
        <div className="item-container">
            <div className="container">
        		<div className="col-sm-5">
  					<div className="product service-image-left">
  						<center>
  							<img id="item-display" src="http://www.corsair.com/Media/catalog/product/g/s/gs600_psu_sideview_blue_2.png" alt=""/>
  						</center>
  					</div>
  				</div>
                <div className="col-sm-7">
  					<div className="product-title">data.title}</div>
  					<div className="product-desc">data.desc}</div>
  					<div className="product-rating">
                        user.ratings}
                        <i className="fa fa-star gold"></i>
                        <i className="fa fa-star gold"></i>
                        <i className="fa fa-star gold"></i>
                        <i className="fa fa-star gold"></i>
                        <i className="fa fa-star-o"></i>
                    </div>
  					<hr/>
  					<div className="product-price">$ service.type.standardPrice}/hr</div>
  					<div className="product-stock">data.isAvailable}</div>
  					<hr/>
  					<div className="btn-group cart">
  						<button type="button" className="btn btn-success">
                            Apply
  						</button>
  					</div>
  					<div className="btn-group wishlist">
  						<button type="button" className="btn btn-danger">
                            data.dateRange}
  						</button>
  					</div>
  				</div>
                <div className="col-sm-12 product-info">
                    <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="DESCRIPTION">
                                <section className="container product-info">
                                    data.otherDetails}
                                    <h3>Corsair Gaming Series GS600 Features:</h3>
                                    <ul>
                                        <li>It supports the latest ATX12V v2.3 standard and is backward compatible with ATX12V 2.2 and ATX12V 2.01 systems</li>
                                        <li>An ultra-quiet 140mm double ball-bearing fan delivers great airflow at an very low noise level by varying fan speed in response to temperature</li>
                                        <li>80Plus certified to deliver 80% efficiency or higher at normal load conditions (20% to 100% load)</li>
                                        <li>0.99 Active Power Factor Correction provides clean and reliable power</li>
                                        <li>Universal AC input from 90~264V — no more hassle of flipping that tiny red switch to select the voltage input!</li>
                                        <li>Extra long fully-sleeved cables support full tower chassis</li>
                                        <li>A three year warranty and lifetime access to Corsair’s legendary technical support and customer service</li>
                                        <li>Over Current/Voltage/Power Protection, Under Voltage Protection and Short Circuit Protection provide complete component safety</li>
                                        <li>Dimensions: 150mm(W) x 86mm(H) x 160mm(L)</li>
                                        <li>MTBF: 100,000 hours</li>
                                        <li>Safety Approvals: UL, CUL, CE, CB, FCC Class B, TÜV, CCC, C-tick</li>
                                    </ul>
                                </section>
                        </Tab>
                        <Tab eventKey={2} title="LOCATION">
                            <section className="container product-info">
                                data.mapLocation}
                            </section>
                        </Tab>
                        <Tab eventKey={3} title="USERS REVIEWS">
                            <section className="container product-info">
                                data.mapLocation}
                            </section>
                        </Tab>
                    </Tabs>


      				<hr/>
                </div>
            </div>
    	</div>
    </div>
    );
  }
}

export default withRouter(ServiceIndex);
