import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import Paper from 'material-ui/Paper';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import events from './events';
import './List.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import ContentMail from 'material-ui/svg-icons/content/mail';


let allViews = Object.keys(BigCalendar.views).map(k => BigCalendar.views[k])


BigCalendar.setLocalizer(
    BigCalendar.momentLocalizer(moment)
);

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
    },
};

const tilesData = [
    {
        img: 'images/grid-list/00-52-29-429_640.jpg',
        title: 'Breakfast',
        author: 'jill111',
    },
    {
        img: 'images/grid-list/burger-827309_640.jpg',
        title: 'Tasty burger',
        author: 'pashminu',
    },
    {
        img: 'images/grid-list/camera-813814_640.jpg',
        title: 'Camera',
        author: 'Danson67',
    },
    {
        img: 'images/grid-list/morning-819362_640.jpg',
        title: 'Morning',
        author: 'fancycrave1',
    },
    {
        img: 'images/grid-list/hats-829509_640.jpg',
        title: 'Hats',
        author: 'Hans',
    },
    {
        img: 'images/grid-list/honey-823614_640.jpg',
        title: 'Honey',
        author: 'fancycravel',
    },
    {
        img: 'images/grid-list/vegetables-790022_640.jpg',
        title: 'Vegetables',
        author: 'jill111',
    },
    {
        img: 'images/grid-list/water-plant-821293_640.jpg',
        title: 'Water plant',
        author: 'BkrmadtyaKarki',
    },
];

/**
 * A simple example of a scrollable `GridList` containing a [Subheader](/#/components/subheader).
 */
const ListService = () => (
    <div>
        <Paper className="profile-sidebar col-sm-12" zDepth={2}>
            <BigCalendar
                {...this.props}
                events={events}
                views={allViews}
                defaultDate={new Date(2015, 3, 1)}
            />
        </Paper>
        <Paper className="profile-sidebar col-sm-12" zDepth={2}>
            <div className="date col-sm-2">
                <h2>25</h2>
                <h5>Apr</h5>
            </div>
            <div className="title col-sm-8">
                <h3>House Cleaning</h3>
                <h5> <strong>Php 100.00/hr</strong> <small>12-5 pm</small></h5>
            </div>
            <div className="col-sm-2" style={{textAlign:"center"}}>
                <Badge
                    badgeContent={10}

                    badgeStyle={{top: 12, right: 12}}
                >
                        <ContentMail />
                </Badge>
            </div>
        </Paper>
    </div>
);

export default ListService;
