import React, {Component} from 'react';
import {
    TextField,
    SelectField,
    MenuItem,
} from 'material-ui';

import { withRouter } from 'react-router-dom';
import "react-image-gallery/styles/css/image-gallery.css";
import Paper from 'material-ui/Paper';

class ServiceInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editorState: "",
            value: "",
            rate: null,
            type: 0,
            event_created: "",
            title: "",
            description: "",
            banner:"",
            time: null,
            location: null,
            isUploading: false,

        };
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange (event, value, type) {
        this.props.handleChange(event, value, type);
    }
    getServices(service) {
        return service.map((obj, int)=> <MenuItem className="type" value={int} key={int} primaryText={obj.name} />)
    }

    render() {
        let rate = !this.props.rate ?  {} : { value: this.props.rate};
        console.log(this.props)
        return (
            <div id='create-post-container' className="">
                <Paper className="profile-sidebar col-sm-12" zDepth={2}>
                    <h3>Tell us more about this service you require</h3>
                    <div className="clearfix">
                        <TextField
                            hintText="Type your title here."
                            floatingLabelText="What needs to be done:"
                            value={this.props.title}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            id="title"
                            onChange={this.handleChange.bind(this)}
                        />
                        <TextField
                            hintText="An Awsome text goes here."
                            floatingLabelText="Describe more details:"
                            value={this.props.description}
                            multiLine={true}
                            rowsMax={10}
                            /*errorText="Title is required"*/
                            fullWidth={true}
                            floatingLabelFixed={true}
                            id="description"
                            onChange={this.handleChange.bind(this)}
                        />
                        <div className="row">
                            <div className="col-xs-6">
                                <SelectField
                                    floatingLabelFixed={true}
                                    floatingLabelText="Pick service type"
                                    value={this.props.type}
                                    id="type"
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth={true}
                                >
                                    {this.getServices(this.props.config.SERVICES)}

                                </SelectField>

                            </div>
                            <div className="col-xs-6">
                                <TextField
                                    hintText="Php 150.00/hour"
                                    floatingLabelText="Rate"
                                    {...rate}
                                    type="number"
                                    id="rate"
                                    onChange={this.handleChange.bind(this)}
                                    /*errorText="Title is required"*/
                                    fullWidth={true}
                                    floatingLabelFixed={true}
                                />
                            </div>
                        </div>
                    </div>
                </Paper>

            </div>
        );
    }

}
export default withRouter(ServiceInfo);
