/* eslint-disable no-undef */
import React, {Component} from 'react';
import {
    TextField,
    SelectField,
    MenuItem,
} from 'material-ui';
import Geosuggest from 'react-geosuggest';
import './ServiceLocation.css';

import { withRouter } from 'react-router-dom';
import "react-image-gallery/styles/css/image-gallery.css";
import Paper from 'material-ui/Paper';

class ServiceLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editorState: "",
            value: "",
            rate: null,
            type: 0,
            event_created: "",
            title: "",
            description: "",
            banner:"",
            time: null,
            location: null,
            isUploading: false,

        };
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange (event, value, type) {
        this.props.handleChange(event, value);
    }

    render() {
        var fixtures = [
            {label: 'Old Elbe Tunnel, Hamburg', location: {lat: 53.5459, lng: 9.966576}},
            {label: 'Reeperbahn, Hamburg', location: {lat: 53.5495629, lng: 9.9625838}},
            {label: 'Alster, Hamburg', location: {lat: 53.5610398, lng: 10.0259135}}
        ];
        return (
            <div id='create-post-container' className="">
                <Paper className="profile-sidebar col-sm-12" zDepth={2}>
                    <h3>Please input your location:</h3>
                    <Geosuggest
                        ref={el=>this._geoSuggest=el}
                        placeholder="Start typing!"
                        initialValue="Hamburg"
                        fixtures={fixtures}
                        onChange={val=>console.log(val)}
                        onSuggestSelect={val=>console.log(val)}
                        location={new google.maps.LatLng(53.558572, 9.9278215)}
                        radius="20" />
                </Paper>

            </div>
        );
    }

}
export default withRouter(ServiceLocation);
