import React, {Component} from 'react';
import {
    TextField,
    SelectField,
    MenuItem,
    Toggle,
    DatePicker,
    Card,
    RaisedButton
} from 'material-ui';

import { withRouter } from 'react-router-dom';
import "react-image-gallery/styles/css/image-gallery.css";
import Paper from 'material-ui/Paper';
// import draftToHtml from 'draftjs-to-html';
import moment from 'moment'
import TimePicker from 'material-ui/TimePicker';
import Divider from 'material-ui/Divider';

class ServiceDateTime extends Component {
    images = [];
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this)
        this.handleDateChange = this.handleDateChange.bind(this)
        this.handleTimeStartChange = this.handleTimeStartChange.bind(this)
        this.handleTimeEndChange = this.handleTimeEndChange.bind(this)
    }
    handleChange (event, value, type) {
        this.props.handleChange(event, value, type)

    }
    handleDateChange(event, value) {
        event = {
            target: {
                id: "service_date"
            }
        };
        this.handleChange(event, value);
    }
    handleTimeStartChange(event, value) {
        event = {
            target: {
                id: "service_time_start"
            }
        };
        this.handleChange(event, value);
    }
    handleTimeEndChange(event, value) {
        event = {
            target: {
                id: "service_time_end"
            }
        };
        this.handleChange(event, value);
    }
    getServices(service) {
        return service.map((obj, int)=> <MenuItem className="type" value={int} key={int} primaryText={obj.name} />)
    }

    render() {
        const showDateTime = this.props.toggle_service_date ? "hidden" : "";

        return (
            <div id='create-post-container' className="">
                <Paper className="profile-sidebar col-sm-12" zDepth={2}>
                    <h3>You can check now or specify a date and time</h3>
                    <div className="clearfix">
                        <Toggle
                            label="Now"
                            id="toggle_service_date"
                            style={{marginBottom:15}}
                            toggled={this.props.toggle_service_date}
                            onToggle={this.handleChange}
                        />
                        <div className={`row ${showDateTime}`}>
                            <div className="col-xs-6">
                                <TextField
                                    hintText={moment().format("DD/MM/YYYY")}
                                    floatingLabelText="What needs to be done:"
                                    value={this.props.service_date}
                                    fullWidth={true}
                                    type="date"
                                    floatingLabelFixed={true}
                                    id="service_date"
                                    onChange={this.handleChange.bind(this)}
                                />
                            </div>

                            <div className="col-xs-3">
                                <TextField
                                    hintText={moment().format("hh:mm")}
                                    floatingLabelText="From"
                                    value={this.props.service_time_start}
                                    fullWidth={true}
                                    type="time"
                                    floatingLabelFixed={true}
                                    id="service_time_start"
                                    onChange={this.handleChange.bind(this)}
                                />

                            </div>

                            <div className="col-xs-3">
                                <TextField
                                    hintText={moment().format("hh:mm")}
                                    floatingLabelText="To"
                                    value={this.props.service_time_end}
                                    fullWidth={true}
                                    type="time"
                                    floatingLabelFixed={true}
                                    id="service_time_end"
                                    onChange={this.handleChange.bind(this)}
                                />

                            </div>

                        </div>
                    </div>
                </Paper>

            </div>
        );
    }

}
export default withRouter(ServiceDateTime);
