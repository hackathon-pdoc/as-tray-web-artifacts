import React, {Component} from 'react';
import {
    TextField,
    SelectField,
    MenuItem,
    FlatButton,
    DatePicker,
    Card,
    RaisedButton
} from 'material-ui';

import { withRouter } from 'react-router-dom';
// import Geosuggest from 'react-geosuggest';
// import Dropzone from 'react-dropzone'
import {s3Upload, invokeArticleAPI} from '../../libs/awsLib'
import {
    Step,
    StepButton,
    Stepper,
    StepLabel,
} from 'material-ui/Stepper';
import moment from 'moment'
import { Editor } from 'react-draft-wysiwyg';
import '../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './Create.css';
import "react-image-gallery/styles/css/image-gallery.css";
import ServiceInfo from './wizard/ServiceInfo'
import ServiceLocation from './wizard/ServiceLocation'
import ServiceDateTime from './wizard/ServiceDateTime'
import Divider from 'material-ui/Divider';

class CreateService extends Component {
    images = [];
    constructor(props) {
        super(props);
        this.state = {
            editorState: "",
            value: "",
            rate: 0,
            type: 0,
            event_created: "",
            title: "",
            description: "",
            banner:"",
            time: null,
            location: null,
            isUploading: false,
            finished: false,
            stepIndex: 0,
            toggle_service_date: true,
            service_date:new Date(),
            service_time_start:moment().format("hh:mm"),
            service_time_end:moment().add(1 ,"hour").format("hh:mm"),
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit() {
        let payload = {
            'title':this.state.title,
            'description':this.state.description,
            'banner':this.state.banner,
            'location':this.state.location,
            'rate':this.state.rate,
            'type':this.state.type,
            'date_create':moment().toISOString(),
            'service_date':JSON.stringify({
                'date': this.state.toggle_service_date ? null : this.state.service_date,
                'service_time_start':this.state.toggle_service_date ? null : this.state.service_time_start,
                'service_time_end':this.state.toggle_service_date ? null : this.state.service_time_end
            })
        }
    }

    handleChange (event, value, type) {

        if(event.target.id) {
            this.setState({[event.target.id]:value}, () => console.log(this.state));
        } else {
            this.setState({type}, () => console.log(this.state));
        }
    }

    handleNext = () => {
        const {stepIndex} = this.state;
        this.setState({
            stepIndex: stepIndex + 1,
            finished: stepIndex >= 3,
        });
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    render() {
        const {finished, stepIndex} = this.state;
        var steps = null;
        switch (stepIndex) {
            case 0:
                steps = (<ServiceInfo {...this.props} {...this.state} handleChange={this.handleChange} />);
                break;
            case 1:
                steps = (<ServiceDateTime {...this.props} {...this.state} handleChange={this.handleChange} />);;
                break;
            case 2:
                steps = (<ServiceLocation {...this.props} handleChange={this.handleChange} />);;
                break;
            case 3:
                steps = (<ServiceInfo {...this.props} handleChange={this.handleChange} />);;
                break
            default:
                steps = ServiceInfo;
                break;
        }
        return (

            <div id='create-post-container' className="">

                <Stepper linear={false} activeStep={stepIndex}>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 0})}>
                            Information
                        </StepButton>
                    </Step>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 1})}>
                            Date and Time
                        </StepButton>
                    </Step>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 2})}>
                            Location
                        </StepButton>
                    </Step>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 3})}>
                            More Details?
                        </StepButton>
                    </Step>
                </Stepper>

                {steps}

                <Divider/>

                <div className="clearfix">
                    <div className="col-sm-6">

                    </div>
                    <div className="col-sm-6 text-right">
                        {finished ? (
                            <p>
                                <a
                                    href="#"
                                    onClick={(event) => {
                                        event.preventDefault();
                                        this.setState({stepIndex: 0, finished: false});
                                    }}
                                >
                                    Click here
                                </a> to reset the example.
                            </p>
                        ) : (
                            <div>
                                <div style={{marginTop: 12}}>
                                    <FlatButton
                                        label="Back"
                                        disabled={stepIndex === 0}
                                        onClick={this.handlePrev}
                                        style={{marginRight: 12}}
                                    />
                                    <RaisedButton
                                        label={stepIndex === 3 ? 'Finish' : 'Next'}
                                        primary={true}
                                        onClick={this.handleNext}
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }

}
export default withRouter(CreateService);
