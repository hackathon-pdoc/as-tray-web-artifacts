import { applyMiddleware, createStore } from 'redux'
import logger from 'redux-logger';
import thunk from 'redux-thunk';
// import thunk from 'redux-thunk';
import {reducer} from './reducer/Reducer'
const middleware = applyMiddleware(logger, thunk);
export default createStore(reducer, middleware);
