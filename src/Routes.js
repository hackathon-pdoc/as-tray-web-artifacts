import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AppliedRoute from './components/AppliedRoute';
import AuthenticatedRoute from './components/AuthenticatedRoute';
import UnauthenticatedRoute from './components/UnauthenticatedRoute';

import Home from './containers/Home';
import Login from './containers/Login';
import Signup from './containers/Signup';
import NotFound from './containers/NotFound';
import ListProfile from './containers/profile/list';
import ServiceIndex from './containers/service/Service';
import CreateService from './containers/service/Create';
import EditProfile from './containers/profile/edit';
import Hackathon from './containers/hackathon/';
import CreatePost from './containers/post/CreatePost';
import DetailsPost from './containers/post/DetailsPost';
import Details from './containers/hackathon/Details';

export default ({ childProps }) => (
  <Switch>
      <AppliedRoute path="/" exact component={Home} props={childProps} />
      <AppliedRoute path="/article/:id" exact component={Details} props={childProps} />
      <UnauthenticatedRoute path="/hackathon" exact component={Hackathon} props={childProps} />
      <UnauthenticatedRoute path="/login" exact component={Login} props={childProps} />
      <UnauthenticatedRoute path="/signup" exact component={Signup} props={childProps} />
      <UnauthenticatedRoute path="/service" exact component={ServiceIndex} props={childProps} />

      <AuthenticatedRoute path="/service" exact component={ServiceIndex} props={childProps} />
      <AuthenticatedRoute path="/profile" exact component={EditProfile} props={childProps} />
      <AuthenticatedRoute path="/profile/edit" exact component={EditProfile} props={childProps} />
      <AuthenticatedRoute path="/post/create" exact component={CreatePost} props={childProps} />
      <AuthenticatedRoute path="/service/create" exact component={CreateService} props={childProps} />
      <AuthenticatedRoute path="/post/:id" exact component={Details} props={childProps} />
    <Route component={NotFound} />
  </Switch>
);
