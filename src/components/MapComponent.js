import React, {PropTypes, Component} from 'react';
import  {connect} from "react-redux"

import GoogleMap from 'google-map-react';
import MyGreatPlace from './MarkersDSA.js';
import {createMarkerLocation} from "../action/ActionMarkers"
class MapComponent extends Component {
    static propTypes = {
        center: PropTypes.array,
        zoom: PropTypes.number,
        greatPlaceCoords: PropTypes.any
    };

    static defaultProps = {
        center: [59.938043, 30.337157],
        zoom: 9,
        greatPlaceCoords: {lat: 59.724465, lng: 30.080121}
    };

    constructor(props) {
        super(props);
    }
    doLoginSignUp() {

    }
    onClick(bol, obj) {
        const {CognitoTokens,  MyGeoLocation } = this.props;
        if (!CognitoTokens.hasSession) {
            this.props.toggleLoginDiablog(true)
            return
        }
        console.log(obj)
        this.props.createMarker(bol, obj);
        return;

    }
    renderMarker(Markers) {
        const marker = []
        Markers.markers.forEach((i, q)=> {
            marker.push(<MyGreatPlace key={q} lat={i.lat} lng={i.lng} />)

        });
        return marker;
    }
    render() {
        const { MyGeoLocation, Markers } = this.props;
        var zoomLevel = parseInt(Math.log2(591657550.5 / (MyGeoLocation.coords.accuracy * 45))) + 1;
        return (
            <GoogleMap
                onClick={(obj)=> this.onClick(true, obj)}
                // apiKey={YOUR_GOOGLE_MAP_API_KEY} // set if you need stats etc ...
                center={this.props.center}
                zoom={zoomLevel}>
                {this.renderMarker(Markers)}
            </GoogleMap>
        );
    }
}
export default MapComponent = connect((store) => {

    return {...store}
})(MapComponent);