import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SearchBar from 'material-ui-search-bar'
import Geosuggest from "react-geosuggest"
import {createMarkerLocation} from "../action/ActionMarkers"
import {connect} from "react-redux"

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
class CreateMarkerDialog extends React.Component {
    constructor(props) {
        super(props);
        let address = this.props.locations.length ? this.props.locations[0].formatted_address : ''
        this.state = {
            open: false,
            name: "",
            description: "",
            address: address,
            lat: props.payload.lat || 0,
            lng: props.payload.lng || 0,
        };
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    locations;
    handleSubmit() {
        const {CognitoTokens} = this.props;
        var payload = Object.assign({}, this.state);
        payload = Object.assign(payload, {
            lat:this.props.payload.lat,
            lng:this.props.payload.lng,
            address: this.props.locations.length ? this.props.locations[0].formatted_address : ''
        });
        this.props.dispatch(createMarkerLocation(payload, CognitoTokens.token));
        this.props.toggleDialog(false);
    };
    componentWillReceiveProps(nextProps) {

    }
    handleClose = () => {
        this.props.toggleDialog(false);
    };
    renderSearchBar() {
        if(!this.props.locations.length) return;
        return <Geosuggest
            style={{
                padding:0,
                margin: '0 auto'
            }}
            initialValue={this.props.locations[0].formatted_address}
        />

        return <SearchBar
            ref={(refs)=>this.SearchBar = refs}
            id="SearchBar"
            value={this.props.locations[0].formatted_address}
            dataSource={this.props.locations.map((obj)=> {
                return obj.formatted_address
            })}
            onChange={() => console.log('onChange')}
            onRequestSearch={() => console.log('onRequestSearch')}

        />
    }

    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <RaisedButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleSubmit}
            />,
        ];

        return (
                <Dialog
                    title={this.renderSearchBar()}
                    actions={actions}
                    modal={true}
                    open={this.props.open}
                    onRequestClose={this.handleClose}
                >
                    <TextField
                        hintText="Title"
                        floatingLabelText="Title"
                        floatingLabelFixed={true}
                        onChange={(e, val)=> this.setState({
                            name: val
                        })}
                    />
                    <TextField
                        hintText="Description"
                        floatingLabelText="Description"
                        floatingLabelFixed={true}
                        onChange={(e, val)=> this.setState({
                            description: val
                        })}
                    />
                </Dialog>
        );
    }
}
CreateMarkerDialog = connect((store) => {
    return {...store}
})(CreateMarkerDialog);

export default CreateMarkerDialog