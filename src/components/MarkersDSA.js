import React, {PropTypes, Component} from 'react';
const  K_WIDTH = 30, K_HEIGHT=45
const greatPlaceStyle = {
    // initially any map object has left top corner at lat lng coordinates
    // it's on you to set object origin to 0,0 coordinates
    position: 'absolute',
    width: K_WIDTH,
    height: K_HEIGHT,
    left: -K_WIDTH / 2,
    top: -K_HEIGHT-15 / 2,
}

export default class MyGreatPlace extends Component {
    render() {

        return (<div style={greatPlaceStyle}>
            <img src="./img/marker-dsa.png" alt=""/>
        </div>)
    }
}