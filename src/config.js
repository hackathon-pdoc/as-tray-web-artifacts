export default {
  MAX_ATTACHMENT_SIZE: 50000000,
  s3: {
    BUCKET: 'pdoc-hackathon-iginite'
  },
  apiGateway: {
    NOTES: {
      URL: 'https://bnojk8doda.execute-api.us-east-1.amazonaws.com/prod',
    },
    PROFILE: {
      URL: 'https://rve2w7n4qb.execute-api.us-east-2.amazonaws.com/prod',
    },
    COMMENTS: {
      URL: 'https://h1cghzebqk.execute-api.us-east-2.amazonaws.com/prod',
    },
      ARTICLE: {
          URL: 'https://rve2w7n4qb.execute-api.us-east-2.amazonaws.com/prod',
      }
  },
    cognito: {
        USER_POOL_ID : 'us-east-2_hkMshTLuu',
        APP_CLIENT_ID : '4ca3i265i4li4f60iovhqersiv',
        REGION: 'us-east-2',
        IDENTITY_POOL_ID: 'us-east-2:1c31af8f-f96b-4572-8888-7f28432c9933',
    },
    SERVICES : [
        {"name": "Cleaner"},
        {"name": "Plumbing"},
        {"name": "Electrician"},
        {"name": "Sitter"},
        {"name": "Gardener"},
        {"name": "Driver"},
        {"name": "Helper"},
        {"name": "Others"}
    ],
    STYlE: {
        //spacing: spacing,
        //fontFamily: 'Roboto, sans-serif',
        palette: {
            primary1Color: "#f36f7d",
            primary2Color: "#b64963",
            primary3Color: "#c0c9ce",
            accent1Color: "#c3b9b0",
            accent2Color: "#c78160",
            accent3Color: "#81675b",
            textColor: "#2f2f2f",
            alternateTextColor: "#ededed",
            canvasColor: "#FFF",//"#e4e6eb",
            /*borderColor: grey300,
            disabledColor: fade(darkBlack, 0.3),
            pickerHeaderColor: cyan500,
            clockCircleColor: fade(darkBlack, 0.07),
            shadowColor: fullBlack,*/
        },
    }

};
