import AWS from 'aws-sdk';
import config from '../config.js';

export async function invokeApig(
  { path,
    method = 'GET',
    body }, userToken) {

  const url = `${config.apiGateway.NOTES.URL}${path}`;
  const headers = {

  };

  body = (body) ? JSON.stringify(body) : body;

  const results = await fetch(url, {
    method,
    body,
    headers
  });

  if (results.status !== 200) {
    throw new Error(await results.text());
  }

  return results.json();
}
export async function invokeProfile(
  { path = '/profile',
    method = 'GET',
    body }, userToken) {

  const url = `${config.apiGateway.PROFILE.URL}${path}`;
  const headers = {
    "Authorization": userToken,
  };

  body = (body) ? JSON.stringify(body) : body;

  const results = await fetch(url, {
    method,
    body,
    headers
  });

  if (results.status !== 200) {
    // throw new results.catch(await results.text());
  }

  return results.json();
}

export async function invokeArticleAPI(
  { path = '/article',
    method = 'GET',
    body }, userToken) {

  const url = `${config.apiGateway.ARTICLE.URL}${path}`;

  const headers = userToken ? {
    "Authorization": userToken,
  } : {};

  body = (body) ? JSON.stringify(body) : body;

  const results = await fetch(url, {
    method,
    body,
    headers
  });

  if (results.status !== 200) {
    return [];
  }

  return results.json();
}

export async function invokeCommentsAPI(
  { path = '/comment',
    method = 'GET',
    body }, userToken) {

  const url = `${config.apiGateway.COMMENTS.URL}${path}`;

  const headers = userToken ? {
    "Authorization": userToken,
  } : {};

  body = (body) ? JSON.stringify(body) : body;

  const results = await fetch(url, {
    method,
    body,
    headers
  });

  if (results.status !== 200) {
    return [];
  }

  return results.json();
}

export function getAwsCredentials(userToken) {
  const authenticator = `cognito-idp.${config.cognito.REGION}.amazonaws.com/${config.cognito.USER_POOL_ID}`;

  AWS.config.update({ region: config.cognito.REGION });

  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: config.cognito.IDENTITY_POOL_ID,
    Logins: {
      [authenticator]: userToken
    }
  });
  console.debug(AWS.config.credentials.getPromise());
  return AWS.config.credentials.getPromise();
}

export async function s3Upload(file, userToken) {
  await getAwsCredentials(userToken);

  const s3 = new AWS.S3({
    params: {
      Bucket: config.s3.BUCKET,
    }
  });
  const filename = `${Date.now()}-${file.name}`;

  return await s3.upload({
    Key: "temp/"+filename,
    Body: file,
    ContentType: file.type,
    ACL: 'public-read',
  }).promise();
}

export async function s3UploadProfile(file, userToken) {
  await getAwsCredentials(userToken);

  const s3 = new AWS.S3({
    params: {
      Bucket: config.s3.BUCKET,
    }
  });
  const filename = `${AWS.config.credentials.identityId}.${file.type}`;

  return await s3.upload({
    Key: "profile/"+filename,
    Body: file,
    ContentType: file.type,
    ACL: 'public-read',
  }).promise();
}
