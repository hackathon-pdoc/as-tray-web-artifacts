const init_location = {
    coords:{},
    isLocationAllowed:false,
    isFetching: true,
    followLocation: true
};




export default function reducer(state=init_location, action) {
    switch (action.type) {
        case "LOCATION_LOADING":
            return {
                ...state,
                isFetching: true,
            };
        case "LOCATION_LOADED":
            return {
                ...action,
                isLocationAllowed:true,
                isFetching: false,
            };
        case "LOCATION_TOGGLE_FOLLOW_LOCATION":
            return {
                ...state,
                followLocation: !action.toggle,
            };
        case "LOCATION_ERROR":
            return {
                ...state,
                isFetching: false,
            };
        case "LOCATION_DENIED":
            return {
                ...state,
                isFetching: false,
            };
        default:
            return state
    }

}