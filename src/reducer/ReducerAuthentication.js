const init_location = {
    message:"",
    error: false,
    isFetching: false,
};




export default function reducer(state=init_location, action) {
    switch (action.type) {
        case "AUTHENTICATION_LOGGING_IN":
            return {
                ...state,
                isFetching: true,
            };
        case "AUTHENTICATION_SUCCESS":
            return {
                ...action,
                message:action.message,
                error: false,
                payload: action.payload,
                isFetching: false,
            };
        case "AUTHENTICATION_FAILURE":
            return {
                ...state,
                message:action.message,
                error: true,
                isFetching: false,
            };
        default:
            return state
    }

}