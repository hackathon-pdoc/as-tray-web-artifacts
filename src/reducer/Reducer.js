import { combineReducers } from 'redux'

import ReducerUserProfile from "./ReducerUserProfile"
import ReducerCognitoTokens from "./ReducerCognitoTokens"
import ReducerMyLocation from "./ReducerMyLocation"
import ReducerAuthentication from "./ReducerAuthentication"
import ReducerMarkers from "./ReducerMarkers"

export const reducer = combineReducers({
    UserProfile: ReducerUserProfile,
    CognitoTokens: ReducerCognitoTokens,
    Markers: ReducerMarkers,
    MyGeoLocation: ReducerMyLocation,
    Authentication: ReducerAuthentication
});