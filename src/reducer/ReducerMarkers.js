const init = {
    markers:[],
    isFetching: false,
    error:{}
};




export default function reducer(state=init, action) {
    switch (action.type) {
        case "MARKER_CREATING":
            return {
                ...state,
                isFetching: true,
            };
        case "MARKER_CREATING_DONE":
            return {
                ...state,
                isFetching: false,
            };
        case "MARKER_CREATING_ERROR":
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        case "MARKER_LOADING":
            return {
                ...state,
                isFetching: true,
            };
        case "MARKER_LOADING_DONE":
            return {
                ...state,
                markers: action.payload,
                isFetching: false,
            };
        case "MARKER_LOADING_ERROR":
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        case "MARKER_RATING_LOADING":
            return {
                ...state,
                isFetching: true,
            };
        case "MARKER_RATING_LOADING_DONE":
            const index = state.markers.findIndex(item => item.id === action.id);
            var temp = state;

            temp.markers[index].comments = action.payload;
            console.log(index)

            return {
                ...temp,
                id:action.id,
                index:index,

                isFetching: false,
            };
        case "MARKER_RATING_LOADING_ERROR":
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        default:
            return state
    }

}