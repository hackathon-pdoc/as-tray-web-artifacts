const init_profile = {
    hasSession: false,
    isLoaded: false,
    isFetching: false,
    userPool: false
}



export default function reducer(state=init_profile, action) {
    switch (action.type) {
        case "FETCH_USER_LOADING":
            return {
                ...state,
                isLoaded: action.isLoaded,
                isFetching: true
            };
        case "FETCH_USER_LOADED":
            return {
                ...state,
                ...action.payload,
                hasSession: true,
                isLoaded: true,
                isFetching: false
            };
        case "FETCH_USER_ERROR":
            return {
                ...state,
                isLoaded: false,
                isFetching: false
            };
        case "FETCH_USER_POOL_NO_SESSION":
            return {
                ...state,
                isLoaded: false,
                isFetching: false
            };
    }
    return state;

}