const init_profile = {
    hasSession: false,
    isLoaded: false,
    isFetching: false,
    userPool: {}
};




export default function reducer(state=init_profile, action) {
    console.log(state);
    switch (action.type) {
        case "FETCH_USER_POOL":
            return {
                ...state,
                isFetching: true,
            };
        case "FETCH_USER_TOKEN_SUCCESS":
            return {
                ...state,
                userPool: action.userPool,
                token:action.token,
                hasSession: true,
                isLoaded: true,
                session:action.session,
                isFetching: false,
            };
        case "FETCH_USER_POOL_NO_SESSION":
            return {
                ...state,
                hasSession: false,
                isLoaded: false,
                isFetching: false,
            };
        case "FETCH_USER_TOKEN_ERROR":
            return {
                ...state,
                hasSession: false,
                isLoaded: false,
                isFetching: false,
            };
        default:
            return state
    }

}