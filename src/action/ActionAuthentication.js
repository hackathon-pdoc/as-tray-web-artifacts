import {
    CognitoUserPool,
    AuthenticationDetails,
    CognitoUser
} from 'amazon-cognito-identity-js';
import config from "../config";


const userPool = new CognitoUserPool({
    UserPoolId: config.cognito.USER_POOL_ID,
    ClientId: config.cognito.APP_CLIENT_ID
});


export function login(username, password) {
    return  function (dispatch) {
        dispatch({type:"AUTHENTICATION_LOGGING_IN"});
        const authenticationData = {
            Username: username,
            Password: password
        };

        const user = new CognitoUser({ Username: username, Pool: userPool });
        const authenticationDetails = new AuthenticationDetails(authenticationData);
        user.authenticateUser(authenticationDetails, {
            onSuccess: (results) => {
                user.getUserAttributes((err, result) => {
                    if (err) {
                        dispatch({type:"AUTHENTICATION_FAILURE", message: err, error:true});
                        return;
                    }
                    let payload = {};
                    for (let i = 0; i < result.length; i++) {
                        payload[result[i].getName()] = result[i].getValue();
                        console.log('attribute ' + result[i].getName() + ' has value ' + result[i].getValue());
                    }
                    dispatch({type:"AUTHENTICATION_SUCCESS", message: results.getIdToken().getJwtToken(), payload:result, ...payload});

                });
            },
            onFailure: (err) => {
                dispatch({type:"AUTHENTICATION_FAILURE", message: err, error:true});
            }
        })
    }
}