import {CognitoUserPool} from 'amazon-cognito-identity-js';
import config from "../config";

export function getUserLocation(callback) {
    return  function (dispatch) {

        dispatch({type:"LOCATION_LOADING"});
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition((position)=>{
                console.debug(position);
                    callback(position);
                    dispatch({type:"LOCATION_LOADED", coords:position.coords});
                },
                (error)=>{
                    if (error.code == error.PERMISSION_DENIED)
                        dispatch({type:"LOCATION_DENIED"});
                });
        } else {
            dispatch({type:"LOCATION_ERROR", error: "Not a modern browser."});

        }
    };
}

export function setFollowLocation(bol) {
    return  function (dispatch) {

        dispatch({type:"LOCATION_TOGGLE_FOLLOW_LOCATION", toggle:bol});

    };
}
