import {CognitoUserPool} from 'amazon-cognito-identity-js';
import config from "../config";



export function createMarkerRating(data, user_token) {
    return  function (dispatch) {
        console.debug(user_token);
        dispatch({type:"MARKER_CREATING_RATING"});
        fetch(config.apiGateway.ARTICLE.URL+"/rating", {
            method: "PUT",
            headers:{
                "Authorization": user_token,
            },
            body: JSON.stringify(data)
        })
        .then((resp)=>resp.json())
        .then((resp)=>{
            dispatch({type:"MARKER_CREATING_RATING_DONE", payload: resp});
        })
        .catch((err)=>{
            dispatch({type:"MARKER_CREATING_RATING_ERROR", error: err});
        });
    };
}

export function createMarkerLocation(data, user_token) {
    return  function (dispatch) {
        dispatch({type:"MARKER_CREATING"});
        console.log(data);
        fetch(config.apiGateway.ARTICLE.URL+"/location", {
            method: "PUT",
            headers:{
                "Authorization": user_token,
            },
            body: JSON.stringify({
                lat: data.lat,
                lng: data.lng,
                description:data.description,
                address:data.address,
                name: data.name
            })
        })
            .then((resp)=>resp.json())
            .then((resp)=>{
            console.log(resp);
                dispatch({type:"MARKER_CREATING_DONE", payload: resp});
            })
            .catch((err)=>{
                dispatch({type:"MARKER_CREATING_ERROR", error: err});
            })
    };
}


export function getMarkerLocation(data, geoLoc) {
    return  function (dispatch) {
        dispatch({type:"MARKER_LOADING"});
        fetch(config.apiGateway.ARTICLE.URL+"/markers/"+ geoLoc.latitude + "/"+ geoLoc.longitude + "/null", {
            method: "GET",
        })
            .then((resp)=>{
            console.log(resp);
                return resp.json()
            })
            .then((resp)=>{
                dispatch({type:"MARKER_LOADING_DONE", payload: resp});
            })
            .catch((err)=>{
                dispatch({type:"MARKER_LOADING_ERROR", error: err});
            })
    };
}
export function getMarkerRatings(id) {
    return  function (dispatch) {
        dispatch({type:"MARKER_RATING__LOADING"});
        fetch(config.apiGateway.ARTICLE.URL+"/rating/"+id, {
            method: "GET",
        })
            .then((resp)=>{
            console.log(resp);
                return resp.json()
            })
            .then((resp)=>{
                dispatch({type:"MARKER_RATING_LOADING_DONE", payload: resp, id:id});
            })
            .catch((err)=>{
                dispatch({type:"MARKER_RATING__LOADING_ERROR", error: err});
            })
    };
}