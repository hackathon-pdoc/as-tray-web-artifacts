import {CognitoUserPool} from 'amazon-cognito-identity-js';
import config from "../config";


const userPool = new CognitoUserPool({
    UserPoolId: config.cognito.USER_POOL_ID,
    ClientId: config.cognito.APP_CLIENT_ID
});



export function getCurrentUser() {
    return function (dispatch) {
        dispatch({type:"FETCH_COGNITO_USER", payload: userPool.getCurrentUser, isLoaded:false})
    }

}


export function getUserToken() {
    return  function (dispatch) {
        var getCurrentUser = userPool.getCurrentUser();
        dispatch({type:"FETCH_USER_POOL"});
        if(!getCurrentUser) {
            dispatch({type:"FETCH_USER_POOL_NO_SESSION"});
            return;
        }

        getCurrentUser.getSession(function (err, session) {
            if (err) {
                dispatch({type:"FETCH_USER_TOKEN_ERROR", error: err});
                return;
            }
            getCurrentUser.get
            dispatch({type:"FETCH_USER_TOKEN_SUCCESS", token: session.getIdToken().getJwtToken(), userPool: userPool, session: session});
        });
    };
}
