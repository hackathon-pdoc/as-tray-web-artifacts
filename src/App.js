import React, {Component} from 'react';
import {
    withRouter,
    Link
} from 'react-router-dom';


import RaisedButton from 'material-ui/RaisedButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import AWS from 'aws-sdk';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CircularProgress from 'material-ui/CircularProgress';
import ProfileSidebar from './containers/profile/sidebar';
import {getUser} from "./action/ActionUserProfile";
import {getCurrentUser, getUserToken} from "./action/ActionAWSCognito";

import AppBar from 'material-ui/AppBar';
import  {connect} from "react-redux"

import Routes from './Routes';
import config from './config.js';
import './App.css';
import SvgIconFace from 'material-ui/svg-icons/action/face';

const styles = {
    chip: {
        margin: 4,
    },
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
};

class App extends Component {
    user = null;
    userPool = null;
    userProfile = null;
    retry=0;
    retryMax=5;

    constructor(props) {
        super(props);

        this.state = {
            userToken: null,
            isLoadingUserToken: true,
            userProfile: {},
            value: 3,
            navClass: '',
            clientHeight:document.body.clientHeight
        };
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop;
            this.setState({
                navClass: (scrollTop>36) ? 'fixed-header' : ""
            })
    }
    async componentDidMount() {
        this.props.dispatch(getUserToken());
        window.addEventListener('scroll', this.handleScroll.bind(this));
        window.addEventListener("resize", ()=> this.setState({clientHeight:document.body.clientHeight}));
        window.scrollTo(0, 0)

    }
    async getProfile(_id) {
        this.props.dispatch(getUser(_id))

        this.setState({
            userProfile: {
                ...this.props.userProfile,
            },
            isLoadingUserToken: false
        });

    }

    handleNavLink = (event) => {
        event.preventDefault();
        this.props.history.push(event.currentTarget.getAttribute('href'));
    }

    handleLogout = (event) => {
        const currentUser = this.userPool;

        if (currentUser !== null) {
            currentUser.signOut();
        }

        if (AWS.config.credentials) {
            AWS.config.credentials.clearCachedId();
        }

        this.updateUserToken(null);

        this.props.history.push('/login');
    }
    renderSkeleton() {
        return <MuiThemeProvider muiTheme={getMuiTheme(config.STYlE)}>
                <div className="container-full content">
                    <CircularProgress style={{
                        margin: "0 auto",
                        position: "absolute",
                        left: 0,
                        right: 0,
                    }} size={60} thickness={7}/>
                </div>
        </MuiThemeProvider>
    }
    render() {
        let {CognitoTokens, UserProfile, dispatch} = this.props;

        const childProps = {
            userToken: CognitoTokens &&  CognitoTokens.token || null,
            user: CognitoTokens,
            updateUserToken: this.updateUserToken,
            profile: UserProfile,
            config: config,
            url: window.location.href.split(window.location.pathname)[0],
            clientHeight:document.body.clientHeight
        };
        const user_avartar = {
            color:"#444",
            icon: <SvgIconFace />

        };

        if(CognitoTokens && (CognitoTokens.isLoaded && UserProfile.userPool)) {
            // dispatch(getUser(CognitoTokens.token));
        }

        return (
                <MuiThemeProvider muiTheme={getMuiTheme()}>
                        <div className="App">
                        {/*<AppBar*/}
                            {/*titleStyle={{zIndex:100, overflow: 'inherit', width:200}}*/}
                            {/*title={ <img*/}
                                {/*onClick={this.handleNavLink}*/}
                                {/*href="/"*/}
                                {/*src="/img/u-servy-logo.png"*/}
                                {/*className="App-logo"*/}
                            {/*/>}*/}
                        {/*/>*/}

                        <div className="container-full content" style={{height:this.state.clientHeight}}>
                                <Routes childProps={childProps}/>
                        </div>
                    </div>
                </MuiThemeProvider>
        )
    }

}
App = connect((store) => {
    return {...store}
})(App)

export default withRouter(App);
